# Changelog

## v0.1.2

  * Added official support for Python 3.9 and 3.10
  * Updated semver to 2.13
  * Updated PyYAML to 6.0

## v0.1.1

  * Increased version number for getting packages uploaded to PyPi

## v0.1.0

  * First Release
